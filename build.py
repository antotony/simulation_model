from room import Room
import json


class Build:

    def __init__(self, filename="data.json"):
        self.rooms = []
        self.read_file("build_variants/" + filename)

    def read_file(self, filename):
        with open(filename, 'r') as f:
            json_data = json.load(f)
            print("input data: %s\n" % json_data)
        for room in json_data["rooms"]:
            room_id = room["id"]
            length = room["length"]
            width = room["width"]
            exits = room["exits"]
            self.rooms.append(
                Room(
                    self, room_id, room["type"], length, width, exits
                )
            )

    def find_room_by_id(self, room_id):
        for i in range(len(self.rooms)):
            if self.rooms[i].id == room_id:
                return self.rooms[i]

    def print_people(self):
        for room in self.rooms:
            print(room.id, end=': ')
            print(len(room.people), end=" [")
            for i in range(len(room.queue)):
                if i != 0:
                    print(end=", ")
                print(len(room.queue[i]), end="")
            print("]")
            # for human in room.people:
            #     print(human.distance_to_exit)
        print()
