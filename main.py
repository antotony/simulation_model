from builtins import range, len
import simpy
from human import Human
from math import sqrt
import matplotlib.pyplot as plt
import threading
from random import random
from build import Build
import sys

TIME_TO_SAVE = 0


def every_one_has_gone(build):
    for room in build.rooms:
        if len(room.people) > 0:
            return False
        for queue in room.queue:
            if len(queue) > 0:
                return False
    return True


def move_all(env, tick, build, graph_data):
    while True:
        graph_data["time"].append(env.now)
        count_gone = 0
        for i in range(len(build.rooms)):
            for j in range(len(build.rooms[i].people)):
                build.rooms[i].people[j].move(build)
            build.rooms[i].remove_people()
            count_gone += build.rooms[i].move_to_the_next_room()
        # print(count_gone)
        graph_data["total_count"].append(count_gone)
        if env.now != 0:
            graph_data["total_count"][env.now] += graph_data["total_count"][env.now - 1]
        # print(env.now)
        # if (env.now + 1) % 5 == 0:
        #     build.print_people()
        if every_one_has_gone(build):
            global TIME_TO_SAVE
            TIME_TO_SAVE = env.now
            break
        yield env.timeout(tick)


def generate_people(total_people, build):
    total_space = 0
    for room in build.rooms:
        total_space += room.length * room.width
    chances = []
    for i in range(len(build.rooms)):
        chances.append(build.rooms[i].length * build.rooms[i].width / total_space * 100)
        if i > 0:
            chances[i] += chances[i - 1]
    for i in range(total_people):
        room_chance = random() * 100
        counter = 0
        while counter < len(chances) and room_chance > chances[counter]:
            counter += 1
        build.rooms[counter].add_human(
            Human(
                build.rooms[counter],
                random() *
                sqrt(pow(build.rooms[counter].length, 2) + pow(build.rooms[counter].width, 2)) /
                len(build.rooms[counter].exits)
            )
        )


def draw_graphs(x, y):
    plt.plot(x, y)
    plt.title("Количество вышедших из здания людей от времени")
    plt.ylabel("Количество людей")
    plt.xlabel("Время (сек)")
    plt.show()


def main(build, sec_lim, total_people, draw_graph=True):
    # plt.plot([1, 2, 3, 4])
    # plt.ylabel('some numbers')
    # plt.show()
    generate_people(total_people, build)
    # build.print_people()
    env = simpy.Environment()
    tick = 1
    graph_data = {"time": [], "total_count": [], "delta": []}
    env.process(move_all(env, tick, build, graph_data))
    env.run(until=sec_lim)
    if draw_graph:
        # plot_graphs = threading.Thread(target=draw_graphs, args=(graph_data["time"], graph_data["total_count"]))
        # plot_graphs.start()
        draw_graphs(graph_data["time"], graph_data["total_count"])
    if not every_one_has_gone(build):
        people_inside = 0
        for room in build.rooms:
            people_inside += len(room.people)
            for i in range(len(room.queue)):
                people_inside += len(room.queue[i])
        print("За", sec_lim, "секунд из здания успели выйти", total_people - people_inside,
              "человек(а) из", total_people)
        build.print_people()
    else:
        if draw_graph:
            print(total_people, "человек выбрались за", TIME_TO_SAVE, "секунд(ы)")


if __name__ == "__main__":
    print(sys.argv)
    time = 60
    people_count = 10
    mode = "default"
    commands = ["-t", "-pc", "-tvpc", "--time-vs-people-count", "--files", "-f"]
    files = []

    # проверка параметров вызова
    try:
        for i in range(len(sys.argv))[1:]:
            if sys.argv[i] not in commands:
                continue
            if sys.argv[i] == '--time-vs-people-count' or sys.argv[i] == '-tvpc':
                mode = "time-vs-people-count"
            if sys.argv[i] == '-t':
                time = int(sys.argv[i + 1])
            if sys.argv[i] == '-pc':
                people_count = int(sys.argv[i + 1])
            if sys.argv[i] == "--files" or sys.argv[i] == "-f":
                j = i + 1
                while j < len(sys.argv) and sys.argv[j] not in commands:
                    files.append(sys.argv[j])
                    j += 1
    except Exception:
        print("Неправильные параметры вызова.\nСхема вызова: python main.py [-t time] [-pc people_count]")
        quit(1)

    if len(files) == 0:
        files = ["data.json"]

    print("Параметры вызова:\n  Время: %s\n  Количество людей: %s\n" % (time, people_count))
    # build = Build("data2.json")
    builds = [Build(file) for file in files]
    if mode == "default":
        for build in builds:
            # main_thread = threading.Thread(target=main, args=(build, time, people_count))
            # main_thread.start()
            main(build, time, people_count)
    elif mode == "time-vs-people-count":
        for build in builds:
            time_vs_people_graph_data = {"time": [], "data": []}
            total_space = 0
            for room in build.rooms:
                total_space += room.length * room.width
            i = 0.05
            while i < 1.51:
                pc = int(i * total_space)
                mean_time = 0
                for j in range(10):
                    # print('\nTest #%i.%i' % (i, j))
                    main(build, 1200, pc, False)
                    mean_time += TIME_TO_SAVE
                mean_time /= 10
                print(i, end="")
                print(":", pc, end="")
                print(":", mean_time)
                time_vs_people_graph_data["time"].append(pc)
                time_vs_people_graph_data["data"].append(mean_time)
                i += 0.05

            counter_green = 0
            counter_orange = 0
            counter_red = 0

            while time_vs_people_graph_data["time"][counter_red] <= 900:
                counter_red += 1
                if time_vs_people_graph_data["time"][counter_green] < 300:
                    counter_green = counter_red
                if time_vs_people_graph_data["time"][counter_orange] < 600:
                    counter_orange = counter_red

            counter_green -= 1
            counter_orange -= 1
            counter_red -= 1

            if counter_red < 0:
                print("Здание опасно. Требуется больше перестрока.")
            elif counter_orange < 0:
                print("Здание небезопасно. Требуется корректировка.")
            elif counter_green < 0:
                print("Здание можно принять. Рекомендована корректировка.")
                print(
                    "Допустимое количесто людей: %i\nПредельное количество людей: %i" % (
                        time_vs_people_graph_data["data"][counter_orange],
                        time_vs_people_graph_data["data"][counter_red]))
            else:
                print(
                    "Безопасное количество людей: %i\nДопустимое количесто людей: %i\nПредельное количество людей: %i" % (
                        time_vs_people_graph_data["data"][counter_green],
                        time_vs_people_graph_data["data"][counter_orange],
                        time_vs_people_graph_data["data"][counter_red]))

            plt.plot(time_vs_people_graph_data["time"], time_vs_people_graph_data["data"], color="black")
            plt.title("Время выхода из здания от количества людей")
            plt.ylabel('Время (сек)')
            plt.xlabel('Количество людей')
            plt.plot(time_vs_people_graph_data["time"], [300 for i in range(len(time_vs_people_graph_data["time"]))],
                     linestyle="--", color="green")
            plt.plot(time_vs_people_graph_data["time"], [600 for i in range(len(time_vs_people_graph_data["time"]))],
                     linestyle="--", color="orange")
            plt.plot(time_vs_people_graph_data["time"], [900 for i in range(len(time_vs_people_graph_data["time"]))],
                     linestyle="--", color="red")
            plt.show()

# TODO: исправить все дистанции OK
# TODO: гистограмма распеделения людей по пройденному расстоянию, говорить, если все плохо
# TODO: сделать серию запусков с меняющимся положением пожара
# TODO: график вышедших за 10 минут от количества людей
# TODO: график времени выхода от количества людей; отменить пунктиром 5, 10 и 15 минут OK
# TODO: сделать генерацию по площади, а не по модели OK
