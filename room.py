from human import Human
from random import random
from random import randint
import math
import copy


class Room:

    def __init__(self, build, room_id, type, length, width, exits):
        self.build = build
        self.id = room_id
        self.type = type
        self.length = length
        self.width = width
        self.people_count = 0
        self.people = []
        self.exits = exits  # exits [{"where": "null", "hpt": 3}] hpt - Human per time, time = 2 sec
        self.queue = [[] for i in range(len(exits))]

    # def generate_people(self):
    #     self.people.extend(
    #         Human(
    #             self.id,
    #             math.sqrt(pow(random() * self.length, 2) + pow(random() * self.width, 2)) / len(self.exits)
    #         )
    #         for _ in range(self.people_count)
    #     )

    def add_to_queue(self, human):
        try:
            self.queue[human.exit_id - 1].append(human)
        except Exception:
            print(human.exit_id - 1, len(self.queue))
            raise Exception

    def remove_people(self):
        i = 0
        count = 0
        while i < len(self.people):
            if self.people[i].distance_to_exit == 0:
                self.people.pop(i)
                i -= 1
            i += 1

    def add_human(self, human):
        self.people.append(human)

    def find_closest_exit(self, indoor):
        """
        Найти ближайшую дверь в комнате к входной двери
        :param indoor: входная дверь пример: {"id": "1", "where": "null", "hpt": 3, "wall": 4, "position": 50}
        :return:
        """

        def distance(room, indoor, outdoor):
            if indoor["wall"] - outdoor["wall"] == 0:
                if indoor["wall"] == 1 or indoor["wall"] == 3:
                    result = float(max(indoor["position"], outdoor["position"]) -
                                   min(indoor["position"], outdoor["position"])) / 100 * room.length
                else:
                    result = float(max(indoor["position"], outdoor["position"]) -
                                   min(indoor["position"], outdoor["position"])) / 100 * room.width
            elif abs(indoor["wall"] - outdoor["wall"]) == 1 or abs(indoor["wall"] - outdoor["wall"]) == 3:
                if indoor["wall"] - outdoor["wall"] == 1 or indoor["wall"] - outdoor["wall"] == -3:
                    if indoor["wall"] == 1 or indoor["wall"] == 3:
                        result = math.sqrt(
                            pow(float(indoor["position"]) / 100 * room.length, 2) +
                            pow(float(100 - outdoor["position"]) / 100 * room.width, 2)
                        )
                    else:
                        result = math.sqrt(
                            pow(float(indoor["position"]) / 100 * room.width, 2) +
                            pow(float(100 - outdoor["position"]) / 100 * room.length, 2)
                        )
                else:
                    if indoor["wall"] == 1 or indoor["wall"] == 3:
                        result = math.sqrt(
                            pow(float(100 - indoor["position"]) / 100 * room.length, 2) +
                            pow(float(outdoor["position"]) / 100 * room.width, 2)
                        )
                    else:
                        result = math.sqrt(
                            pow(float(100 - indoor["position"]) / 100 * room.width, 2) +
                            pow(float(outdoor["position"]) / 100 * room.length, 2)
                        )
            else:
                if indoor["wall"] == 1 or indoor["wall"] == 3:
                    result = math.sqrt(
                        pow(room.width, 2) +
                        pow(float(max(indoor["position"], outdoor["position"]) -
                                  min(indoor["position"], outdoor["position"])) / 100 * room.length, 2))
                else:
                    result = math.sqrt(
                        pow(room.length, 2) +
                        pow(float(max(indoor["position"], outdoor["position"]) -
                                  min(indoor["position"], outdoor["position"])) / 100 * room.width, 2))
            if result < 3:
                result += 0
            return result

        min_dist = -1000
        res_dist = -1
        number = -1
        room = self.build.find_room_by_id(indoor["where"])
        # if room.id == "102" and len(room.queue[0]) > 0:
        #     print("test")
        for i in range(len(self.build.find_room_by_id(indoor["where"]).exits)):
            door = room.exits[i]
            dist = distance(self.build.find_room_by_id(indoor["where"]), indoor, door)
            exit_coef = 0
            if door["where"] == "null":
                exit_coef = -30
            if dist + len(room.queue[i])/2 + exit_coef < min_dist or min_dist < 0:
                min_dist = dist + len(room.queue[i])/2 + exit_coef
                res_dist = dist
                number = i
            elif dist + len(room.queue[i])/2 == min_dist:
                if random() < 0.5:
                    number = i
        return res_dist, number + 1

    def move_to_the_next_room(self):
        """
        Перемещение людей из очереди на выход в следующую комнату
        """
        out = copy.deepcopy(self.exits)
        count = 0
        for i in range(len(self.queue)):
            for j in range(min(len(self.queue[i]), out[i]["hpt"])):
                human = self.queue[i].pop(0)
                if out[i]["where"] != "null":
                    door = copy.deepcopy(out[i])
                    door["wall"] -= 2
                    if door["wall"] == 0:
                        door["wall"] = 4
                    if door["wall"] == -1:
                        door["wall"] = 3
                    door["position"] = 100 - door["position"]
                    human.distance_to_exit, human.exit_id = self.find_closest_exit(door)
                    # print(human.distance_to_exit)
                    human.room = self.build.find_room_by_id(out[i]["where"])
                    if human.room.type == "stair":
                        human.speed *= 0.5
                    else:
                        human.speed *= 2
                    self.build.find_room_by_id(out[i]["where"]).add_human(human)
                else:
                    count += 1
        return count
