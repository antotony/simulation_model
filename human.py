from random import random
from random import randint


class Human:

    def __init__(self, room, distance_to_exit, speed=1.0, exit_id=0):
        self.room = room
        self.distance_to_exit = distance_to_exit
        # print("Создан в %s, расстояние: %f" %(room.id, distance_to_exit))
        if exit_id == 0:
            self.exit_id = randint(0, len(room.exits) - 1) + 1
        else:
            self.exit_id = exit_id
        self.time_in_build = 0
        coef = 0.75
        self.speed = (speed + speed * random()) * coef

    def move(self, build):
        if self.distance_to_exit <= self.speed:
            self.distance_to_exit = 0
            self.room.add_to_queue(self)
            # print("Вышел из: %s за %s" % (self.room.id, self.time_in_build))
        else:
            self.distance_to_exit -= self.speed
            self.time_in_build += 1
            # print("in %s: %i" % (self.room.id, self.time_in_build))

    def get_distance(self):
        return self.distance_to_exit
